import os
import argparse
import numpy as np
import uuid
from matplotlib.pyplot import imread
import cv2
from matplotlib import pyplot as plt

# get previous tensorflow version
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

os.environ['CUDA_VISIBLE_DEVICES'] = '0'

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# input image path
parser = argparse.ArgumentParser()

parser.add_argument('--im_path', type=str, default='./demo/45765448.jpg',
                    help='input image paths.')

# color map
floorplan_map = {
	0: [255,255,255], # background
	1: [192,192,224], # closet
	2: [192,255,255], # batchroom/washroom
	3: [224,255,192], # livingroom/kitchen/dining room
	4: [255,224,128], # bedroom
	5: [255,160, 96], # hall
	6: [255,224,224], # balcony
	7: [255,255,255], # not used
	8: [255,255,255], # not used
	9: [255, 60,128], # door & window
	10:[  0,  0,  0]  # wall
}

def ind2rgb(ind_im, ff, color_map=floorplan_map):
	rgb_im = np.zeros((ind_im.shape[0], ind_im.shape[1], 3))
	wall_im = np.ones((ind_im.shape[0], ind_im.shape[1], 3))*255

	for i, rgb in color_map.items():
		ff.write("\nI= "+ str(i)+": "+ str(rgb_im[(ind_im==i)])+ "+RGB: "+ str(rgb))

		rgb_im[(ind_im==i)] = rgb
		if i == 10 :
			print("here")
			wall_im[(ind_im==10)] = [0,0,0]
		if i == 9 :
			print("here2")
			wall_im[(ind_im==9)] = [0,255,0]


	return rgb_im, wall_im

def main(args):
	# load input
	im = imread(args.im_path)
	im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
	im = im.astype(np.float32)
	im = cv2.resize(src=im, dsize=(512,512), interpolation=cv2.INTER_CUBIC) / 255.

	# create tensorflow session
	with tf.Session() as sess:
		fileStr = args.im_path.split("/")[-1]
		f= open("./results/output_"+fileStr+".txt","w+")
		# initialize
		sess.run(tf.group(tf.global_variables_initializer(),
					tf.local_variables_initializer()))

		# restore pretrained model
		saver = tf.train.import_meta_graph('./pretrained/pretrained_r3d.meta')
		saver.restore(sess, './pretrained/pretrained_r3d')

		# get default graph
		graph = tf.get_default_graph()

		# restore inputs & outpus tensor
		x = graph.get_tensor_by_name('inputs:0')
		room_type_logit = graph.get_tensor_by_name('Cast:0')
		room_boundary_logit = graph.get_tensor_by_name('Cast_1:0')

		# infer results
		[room_type, room_boundary] = sess.run([room_type_logit, room_boundary_logit],\
										feed_dict={x:im.reshape(1,512,512,3)})
		f.write("\n Infer results \n")
		f.write("room type: "+str(room_type))
		f.write("\nroom boundary: "+str(room_boundary))
		room_type, room_boundary = np.squeeze(room_type), np.squeeze(room_boundary)
		f.write("\n AFTER; \n room type: "+str(room_type))
		f.write("\nroom boundary: "+str(room_boundary))

		# merge results
		f.write("\n\nMerged floorplan")
		floorplan = room_type.copy()
		floorplan[room_boundary==1] = 9
		floorplan[room_boundary==2] = 10
		f.write("\nbefore: "+str(floorplan))
		floorplan_rgb, wall_rgb = ind2rgb(floorplan, f)
		f.write("\nRGB: "+str(floorplan_rgb))


		# plot results
		plt.subplot(221)
		plt.imshow(im)
		plt.subplot(222)
		plt.imshow(floorplan_rgb/255.)
		plt.subplot(223)
		plt.imshow(floorplan)
		plt.subplot(224)
		plt.imshow(wall_rgb/255.)
		plt.show()
		strPlan = args.im_path.split("/")[-1]
		plt.savefig('./results/'+strPlan)
		f.close()

if __name__ == '__main__':
	FLAGS, unparsed = parser.parse_known_args()
	main(FLAGS)
